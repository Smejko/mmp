<link href="../CSS/style.css" rel="stylesheet" type="text/css"/>
<link href="../CSS/Index.css" rel="stylesheet" type="text/css"/>
<link href="../CSS/test.css" rel="stylesheet" type="text/css"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>MMP</title>
    </head>
    <body>
        <div id="main">
    <div id="header">
      <div id="logo">
      <input type="image" style="width: 450px" style= "height: 450px" style="align: center" src="../slike/logo_mmp.jpg" alt=""/>
      </div>
      <div id="menubar">
        <ul id="menu">
          <li class="selected"><a href="index.php">Vozni redi</a></li>
          <li><a href="../HTML/cenik.html">Cenik</a></li>
          <li><a href="../HTML/mreza_linij.html">Mreža linij</a></li>
          <li><a href="../HTML/obvestila.html">Obvestila</a></li>
          <li> <a href="../HTML/o_strani.html">O spletni strani</a></li>
          <li><a href="../HTML/kontakt.html">Kontakt</a></li>
          
        </ul>
      </div>
    </div>
    <div id="site_content">
      <div class="sidebar">
        <h3>Povezave</h3>
        <p><a href="https://www.facebook.com/MMP-Mariborski-mestni-prevozi-1516327975359920/?fref=ts"><img src="../slike/index.png" alt="" width="42" height="42"/></a>- Facebook</p>
        
        <hr>
        <br>
        <p><a href="https://twitter.com/mestni_prevozi"><img src="../slike/twitter.png" alt="" width="42" height="42"/></a>- Twitter</p>
        
        <h3>Iskanje</h3>
        <form method="post" action="#" id="search_form">
          <p>
            <input class="search" type="text" name="search_field" value="Vnesi besedilo..." />
            <input name="search" type="image" style="border: 0; margin: 0 0 -9px 5px;" src="../slike/search.png" alt="Search" title="Search" />
          </p>
        </form>
      </div>
      <div id="content">
          <h1>Izberi postajo:</h1>
          <form name="submitPostaj" action="prikaz.php" method="POST">
            <select name="Vstopna">
                <option value="">Vstopna postaja</option>
                
                 <option id="bold" value="">• LINIJA 1 Tezenska Dobrava Smer AP Mlinska - Tezenska dobrava</option>
                 <option value="Melje obracalisce"> - Melje Obracalisce</option>
                 <option value="Meljska Trgovina"> - Meljska Trgovina</option>
                 <option value="Meljska Partizanska">- Meljska Partizanska</option>
                 <option value="AP Mlinska">- AP Mlinska</option>
                 <option value="City Center"> - City Center</option>
                 <option value="Kneza Koclja - Vetrinjska"> - Kneza Koclja - Vetrinjska</option>
                 <option value="Magdalenski Park"> - Magdalenski Park</option>
                 <option value="Ljubljanska - Pariške komune"> - Ljubljanska - Pariške komune</option>
                 <option value="Titova - Nasipna"> - Titova - Nasipna</option>
                 <option value="Ptujska - Tržaška"> - Ptujska - Tržaška</option>
                 <option value="Ptujska - hitra cesta">- Ptujska - hitra cesta</option>
                 <option value="Ptujska - Kovinar">- Ptujska - Kovinar</option>
                 <option value="Ptujska - Autocommerce">- Ptujska - Autocommerce</option>
                 <option value="Ptujska - Posta">- Ptujska - Pošta</option>
                 <option value="Ptujska - TAM">- Ptujska - TAM</option>
                 <option value="Dogoska - vrtec">- Dogoška - vrtec</option>
                 <option value="KS Silvira Tomassini">- KS Silvira Tomassini</option>
                 <option value="Tezenska Dobrava - obracalisce">- Tezenska Dobrava - obračalisče</option>

                 
                 <option id="bold" value="LINIJA 2 Betnavska-Razvanje">• LINIJA 2 Betnavska-Razvanje</option>
                 <option value="Melje - obracalisce">- Melje - obračališče</option>
                 <option value="Ul. kraljevica Marka">- Ul. kraljeviča Marka</option> 
                 <option value="Oreško nabrežje 1">- Oreško nabrežje 1</option> 
                 <option value="Oreško nabrežje 2">- Oreško nabrežje 2</option> 
                 <option value="AP Mlinska">- AP Mlinska</option> 
                 <option value="City center">- City center</option> 
                 <option value="Kneza Koclja - Vetrinjska">- Kneza Koclja - Vetrinjska</option> 
                 <option value="Tabor">- Tabor</option> 
                 <option value="Betnavska - Žolgarjeva ">- Betnavska - Žolgarjeva </option> 
                 <option value="Betnavska - Focheva ">- Betnavska - Focheva </option> 
                 <option value="Betnavska - Metelkova">- Betnavska - Metelkova </option> 
                 <option value="Goriška ">- Goriška </option> 
                 <option value="Betnavska - Knafelčeva">- Betnavska - Knafelčeva </option> 
                 <option value="Kardeljeva - Knafelčeva">- Kardeljeva - Knafelčeva</option> 
                 <option value="Kardeljeva - OŠ Tabor I.">- Kardeljeva - OŠ Tabor I.</option> 
                 <option value="Kardeljeva - Borštnikova">- Kardeljeva - Borštnikova</option> 
                 <option value="Betnavski grad Razvanje - vrtnarstvo">- Betnavski grad Razvanje - vrtnarstvo</option> 
                 <option value="Razvanje - obracalisce">- Razvanje - GD Razvanje - obračališče</option> 
                 
                 
                 
                 
                 <option id="bold" value="LINIJA 3 Dobrava-Tezno-Gosposvetska rondo-AP Mlinska-Dobrava">• LINIJA 3 Dobrava-Tezno-Gosposvetska rondo-AP Mlinska-Dobrava</option>
                 <option value="-Pokopopališče Dobrava - vhod"> -Pokopopališče Dobrava - vhod</option>
                 <option value="-Pokopališče Dobrava - obračališče"> -Pokopališče Dobrava - obračališče</option>
                 
                 
                 <option value="LINIJA 4 Studenci"> LINIJA 4 Studenci</option>
                 <option value="-AP Mlinska"> -AP Mlinska</option>
                 <option value="-Limbuš - Marof"> -Limbuš - Marof</option>
                 
                 <option value="LINIJA 6 Vzpenjača"> LINIJA 6 Vzpenjača</option>
                 <option value="-Melje - obračališče"> -Melje - obračališče</option>
                 <option value="-Vzpenjača - obračališče"> -Vzpenjača - obračališče</option>
                 
                 <option id="bold" value="">• LINIJA 7 Kamnica</option>
		 <option value="7.Melje obračališče"> - Melje - obračališče</option>
                 <option value="7.Meljska trgovina"> - Meljska - trgovina</option>
                 <option value="7.Meljska Partizanska"> - Meljska - Partizanska</option>
                 <option value="7.AP Mlinska"> - AP Mlinska</option>
                 <option value="7.Krekova občina"> - Krekova - občina</option>
                 <option value="7.Krekova"> - Krekova</option>
                 <option value="7.Stadion Ljudski vrt"> - Stadion - Ljudski vrt</option>
                 <option value="7.Vrbanska šola"> - Vrbanska šola</option>
                 <option value="7.Kamnica hipodrom"> - Kamnica - hipodrom</option>
                 <option value="7.Kamnica šola"> - Kamnica - šola</option>
                 <option value="7.Kamnica pošta"> - Kamnica - pošta</option>
                 <option value="7.Kamnica trgovina"> - Kamnica - trgovina</option>
                 <option value="7.Cesta v Rošpoh 65"> - Cesta v Rošpoh 65</option>
                 <option value="7.Cesta v Rošpoh 115"> - Cesta v Rošpoh 115</option>
                 <option value="7.Rošpoh odcep Urban"> - Rošpoh - odcep Urban</option>
		 <option value="7.Rošpoh Hojnik"> - Rošpoh - Hojnik</option>
		 <option value="7.Rošpoh obračališče "> - Rošpoh - obračališče </option>

                
                <option id="bold" value="">• LINIJIA ŠT 8: AP Mlisnak - Terme fontana</option>
                <option value="8.AP Mlinska"> - AP Mlinska </option>
                <option value="8.Krekova občina"> - Krekova občina</option>
                <option value="8.Gregorčičeva"> - Gregorčičeva</option>
                <option value="8.Krekova"> - Krekova </option>
                <option value="8.Tretja gimnazija"> - Tretja gimnazija </option>
                <option value="8.Gosposvetska vrbanska"> - Gosposvetska - Vrbanska </option>
                <option value="8.Gosposvetska turnerjeva"> - Gosposvetska - Turnerjeva</option>
                <option value="8.Gosposvetska rondo"> - Gosposvetska - Rondo </option>
                <option value="8.Terme fontana"> - Terme Fontana</option>
                 
                 <option id="bold" value="">• LINIJIA ŠT 9: Zrkovci - Dogoše</option>
                <option value="9.AP Mlinska"> - AP Mlinska</option>
                <option value="9.Citiy center"> - Citiy Center</option>
                <option value="9.Kneza koclja vetrinjska"> - Kneza Koclja - Vetrinjska</option>
                <option value="9.Magdalena"> - Magdalena</option>
                <option value="9.Pobreška europark"> - Pobreška - Europark</option>
                <option value="9.Greenwich"> - Greenwich</option>
                <option value="9.Čufarjeva TVD partizan"> - Čufarjeva - TVD Partizan</option>
                <option value="9.Čufarjeva dom st.občanov"> - Čufarjeva - dom st. občanov</option>
                <option value="9.Zrkovska GD"> - Zrkovska GD</option>
                <option value="9.Zrkovska cesta"> - Zrkovska cesta</option>
                <option value="9.Zrkovska trgovina"> - Zrkvoska trgovina</option>
                <option value="9.Zrkovci na terasi"> - Zrkvoci - Na terasi</option>
                <option value="9.Zrkovci obračališče"> - Zrkvoci - obračališe</option>
                <option value="9.Zrkovci na gorci 54"> - Zrkvoci - Na gorci 54</option>
                <option value="9.Zrkovci na gorci 65"> - Zrkvoci - Na gorci 65</option>
                <option value="9.Svenškova ulica 40"> - Svenškova ulica 40</option>
                <option value="9.Dupleška cesta 239"> - Dupleška cesta 239</option>
                <option value="9.Dogoše GD"> - Dogoše - GD</option>
                <option value="9.Dupleška cesta 255"> - Dupleška cesta 255</option>
                 
                 
                 
                 
                <option id="bold" value="">• LINIJIA ŠT 10: Malečnik </option>
                <option value="10.Melje obračališče"> - Melje obračališče</option>
                <option value="10.Ul.kraljeviča marka"> - Ul.Kraljeviča Marka</option>
                <option value="10.Oreško nabrežje 1"> - Oreško nabrežje 1</option>
                <option value="10.Oreško nabrežje 2"> - Oreško nabrežje 2</option>
                <option value="10.AP mlinska"> - AP Mlinska</option>
                <option value="10.City center"> - City center</option>
                <option value="10.Kneza koclja vetrinjska"> - Kneza Koclja - Vetrinjska</option>
                <option value="10.Magdalena"> - Magdalena</option>
                <option value="10.Pobreška europark"> - Pobreška Europark</option>
                <option value="10.Greenwich"> - Greenwich</option>
                <option value="10.Čufarjeva TVD partizan"> - Čufarjeva - TVD Partizan</option>
                <option value="10.Čufarjeva dom st.občanov"> - Čufarjeva - dom st. občanov</option>
                <option value="10.Meljski hrib most"> - Meljski hrib - most</option>
                <option value="10.Kovačič"> - Kovačič</option>
                <option value="10.Lorber"> - Lorber</option>
                <option value="10.Malečnik odcep trčova"> - Malečnik - odcep Trčova</option>
                <option value="10.Malečnik trgovina"> - Malečnik trgovina</option>
                <option value="10.Trčova 31a"> - Trčova 31a</option>
                <option value="10.Trčova griček"> - Trčova - Griček</option>
                <option value="10.Novak"> - Novak</option>
                <option value="10.Metava duplek križišče"> - Metava - Duplek križišče</option>
                <option value="10.Metava obračališče"> - Metava - obračališče</option>
                 
                 <option value="LINIJA 12 Dobrava-Pobrežje-AP Mlinska-Gosposvetska rondo-Dobrava "> LINIJA 12 Dobrava-Pobrežje-AP Mlinska-Gosposvetska rondo-Dobrava</option>
                 <option value="-Pokopališče Dobrava - obračališče"> -Pokopališče Dobrava - obračališče</option>
                 <option value="-Dupleška - kanal "> -Dupleška - kanal </option>
                 
                 <option id="bold" value="LINIJA 13 Črnogorska ">• LINIJA 13 Črnogorska</option>
                 <option value="Poštni center - obračališče"> -Poštni center - obračališče</option>
                 <option value="Tam - Nkbm"> -Tam - Nkbm</option>
                 <option value="Tam - črpalka"> -Tam - črpalka</option> 
                 <option value="Tam - Durabus"> -Tam - Durabus</option> 
                 <option value="Tam - vhod"> -Tam - vhod</option> 
                 <option value="Perhavčeva ul. - šola"> -Perhavčeva ul. - šola</option> 
                 <option value="Kavčičeva"> -Kavčičeva</option> 
                 <option value="Zagrebška - Šerbinek"> -Zagrebška - Šerbinek</option> 
                 <option value="Zagrebška - Kovinar"> -Zagrebška - Kovinar</option> 
                 <option value="ŽP Tezno"> -ŽP Tezno</option> 
                 <option value="Belokranjska ulica"> -Belokranjska ulica</option> 
                 <option value="Makedonska"> -Makedonska</option> 
                 <option value="Črnogorska št. 23"> -Črnogorska št. 23</option> 
                 <option value="Ob gozdu"> -Ob gozdu</option> 
                 <option value="Nasipna - Snaga"> -Nasipna - Snaga</option> 
                 <option value="Pobreška - Europark"> -Pobreška - Europark</option> 
                 <option value="Magdalena"> -Magdalena</option> 
                 <option value="Glavni trg - Židovska"> -Glavni trg - Židovska</option> 
                 <option value="City center"> -City center</option> 
                 <option value="AP Mlinska"> -AP Mlinska</option>

 
                 <option value="-AP Mlinska"> -AP Mlinska</option>
                 
                 <option value="LINIJA 15 Bresternica "> LINIJA 15 Bresternica </option>
                 <option value="-Bresternica - obračališče"> -Bresternica - obračališče</option>
                 <option value=" -Košaški dol">  -Košaški dol</option>
                 
                 <option value="LINIJA 151 Gaj nad Mariborom "> LINIJA 151 Gaj nad Mariborom </option>
                 <option value="-Gaj nad Mariborom"> -Gaj nad Mariborom</option>
                 <option value="-AP Mlinska"> -AP Mlinska</option>
                 
                 <option value="LINIJA 16 Dogoše-Zg. Duplek "> LINIJA 16 Dogoše-Zg. Duplek</option>
                 <option value="-AP Mlinska"> -AP Mlinska</option>
                 <option value="-Zg.Duplek - obračališče"> -Zg.Duplek - obračališče</option>
                 
                 <option value="LINIJA 17: Ribniško selo - Studenci "> LINIJA 17: Ribniško selo - Studenci </option>
                 <option value="-Ribniško selo - obračališče">-Ribniško selo - obračališče</option>
                 <option value="-Studenci - obračališče"> -Studenci - obračališče</option>
                 
                 <option value="LINIJA 18 Pekre "> LINIJA 18 Pekre</option>
                 <option value="-Melje - obračališče"> -Melje - obračališče</option>
                 <option value="-Pekre - trgovina"> -Pekre - trgovinae </option>
                 
                 <option value="LINIJA 19 Šarhova ">LINIJA 19 Šarhova</option>
                 <option value="-AP Mlinska"> -AP Mlinska</option>
                 <option value="-Šarhova - obračališče"> -Šarhova - obračališče </option>
                 
                  <option value="LINIJA 20 Grušova "> LINIJA 20 Grušova</option>
                 <option value="-Melje - obračališče"> -Melje - obračališče</option>
                 <option value=" -Grušova-obračališče">  -Grušova-obračališče </option>
                 
                 <option value="LINIJA 21 Ljubljanska-Tržaška c. - Merkur "> LINIJA 21 Ljubljanska-Tržaška c. - Merkur</option>
                 <option value="-Melje - obračališče"> -Melje - obračališče</option>
                 <option value="- Tržaška cesta - Merkur">  -Tržaška cesta - Merkur </option>
            </select>
 
          <div>
              <img src="../slike/arrow1.png" alt="" id="arrow1"/>
          </div>
          
            <select name="Izstopna">
                 <option value="">Izstopna postaja</option>
                  <option id="bold" value="">• LINIJA 1 Tezenska Dobrava Smer AP Mlinska - Tezenska dobrava</option>
                 <option value="Melje obracalisce"> - Melje Obracalisce</option>
                 <option value="Meljska Trgovina"> - Meljska Trgovina</option>
                 <option value="Meljska Partizanska">- Meljska Partizanska</option>
                 <option value="AP Mlinska">- AP Mlinska</option>
                 <option value="City Center"> - City Center</option>
                 <option value="Kneza Koclja - Vetrinjska"> - Kneza Koclja - Vetrinjska</option>
                 <option value="Magdalenski Park"> - Magdalenski Park</option>
                 <option value="Ljubljanska - Pariške komune"> - Ljubljanska - Pariške komune</option>
                 <option value="Titova - Nasipna"> - Titova - Nasipna</option>
                 <option value="Ptujska - Tržaška"> - Ptujska - Tržaška</option>
                 <option value="Ptujska - hitra cesta">- Ptujska - hitra cesta</option>
                 <option value="Ptujska - Kovinar">- Ptujska - Kovinar</option>
                 <option value="Ptujska - Autocommerce">- Ptujska - Autocommerce</option>
                 <option value="Ptujska - Posta">- Ptujska - Pošta</option>
                 <option value="Ptujska - TAM">- Ptujska - TAM</option>
                 <option value="Dogoska - vrtec">- Dogoška - vrtec</option>
                 <option value="KS Silvira Tomassini">- KS Silvira Tomassini</option>
                 <option value="Tezenska Dobrava - obracalisce">- Tezenska Dobrava - obračalisče</option>
                 
                 
                <option id="bold" value="LINIJA 2 Betnavska-Razvanje">• LINIJA 2 Betnavska-Razvanje</option>
                 <option value="Melje - obracalisce">- Melje - obračališče</option>
                 <option value="Ul. kraljevica Marka">- Ul. kraljeviča Marka</option> 
                 <option value="Oreško nabrežje 1">- Oreško nabrežje 1</option> 
                 <option value="Oreško nabrežje 2">- Oreško nabrežje 2</option> 
                 <option value="AP Mlinska">- AP Mlinska</option> 
                 <option value="City center">- City center</option> 
                 <option value="Kneza Koclja - Vetrinjska">- Kneza Koclja - Vetrinjska</option> 
                 <option value="Tabor">- Tabor</option> 
                 <option value="Betnavska - Žolgarjeva ">- Betnavska - Žolgarjeva </option> 
                 <option value="Betnavska - Focheva ">- Betnavska - Focheva </option> 
                 <option value="Betnavska - Metelkova">- Betnavska - Metelkova </option> 
                 <option value="Goriška ">- Goriška </option> 
                 <option value="Betnavska - Knafelčeva">- Betnavska - Knafelčeva </option> 
                 <option value="Kardeljeva - Knafelčeva">- Kardeljeva - Knafelčeva</option> 
                 <option value="Kardeljeva - OŠ Tabor I.">- Kardeljeva - OŠ Tabor I.</option> 
                 <option value="Kardeljeva - Borštnikova">- Kardeljeva - Borštnikova</option> 
                 <option value="Betnavski grad Razvanje - vrtnarstvo">- Betnavski grad Razvanje - vrtnarstvo</option> 
                 <option value="Razvanje - obracalisce">- Razvanje - GD Razvanje - obračališče</option> 
                 
                 
                 <option value="LINIJA 3 Dobrava-Tezno-Gosposvetska rondo-AP Mlinska-Dobrava">LINIJA 3 Dobrava-Tezno-Gosposvetska rondo-AP Mlinska-Dobrava</option>
                 <option value="-Pokopopališče Dobrava - vhod">-Pokopopališče Dobrava - vhod</option>
                 <option value="-Pokopališče Dobrava - obračališče"> -Pokopališče Dobrava - obračališče</option>
                 
                 
                 <option value="LINIJA 4 Studenci">LINIJA 4 Studenci</option>
                 <option value="-AP Mlinska"> -AP Mlinska</option>
                 <option value="-Limbuš - Marof"> -Limbuš - Marof</option>
                 
                 
                 <option value="LINIJA 6 Vzpenjača"> "LINIJA 6 Vzpenjača</option>
                 <option value="-Melje - obračališče"> -Melje - obračališče</option>
                 <option value="-Vzpenjača - obračališče"> -Vzpenjača - obračališče</option>
                 
                 
                 <option id="bold" value="">• LINIJA 7 Kamnica</option>
		 <option value="Melje obračališče"> - Melje - obračališče</option>
                 <option value="Meljska trgovina"> - Meljska - trgovina</option>
                 <option value="Meljska Partizanska"> - Meljska - Partizanska</option>
                 <option value="AP Mlinska"> - AP Mlinska</option>
                 <option value="Krekova občina"> - Krekova - občina</option>
                 <option value="Krekova"> - Krekova</option>
                 <option value="Stadion Ljudski vrt"> - Stadion - Ljudski vrt</option>
                 <option value="Vrbanska šola"> - Vrbanska šola</option>
                 <option value="Kamnica hipodrom"> - Kamnica - hipodrom</option>
                 <option value="Kamnica šola"> - Kamnica - šola</option>
                 <option value="Kamnica pošta"> - Kamnica - pošta</option>
                 <option value="Kamnica trgovina"> - Kamnica - trgovina</option>
                 <option value="Cesta v Rošpoh 65"> - Cesta v Rošpoh 65</option>
                 <option value="Cesta v Rošpoh 115"> - Cesta v Rošpoh 115</option>
                 <option value="Rošpoh odcep Urban"> - Rošpoh - odcep Urban</option>
		 <option value="Rošpoh Hojnik"> - Rošpoh - Hojnik</option>
		 <option value="Rošpoh obračališče "> - Rošpoh - obračališče </option>
                 
                 
                 <option id="bold" value="">• LINIJIA ŠT 8: AP Mlisnak - Terme fontana</option>
                <option value="8.AP Mlinska"> - AP Mlinska </option>
                <option value="8.Krekova občina"> - Krekova občina</option>
                <option value="8.Gregorčičeva"> - Gregorčičeva</option>
                <option value="8.Krekova"> - Krekova </option>
                <option value="8.Tretja gimnazija"> - Tretja gimnazija </option>
                <option value="8.Gosposvetska vrbanska"> - Gosposvetska - Vrbanska </option>
                <option value="8.Gosposvetska turnerjeva"> - Gosposvetska - Turnerjeva</option>
                <option value="8.Gosposvetska rondo"> - Gosposvetska - Rondo </option>
                <option value="8.Terme fontana"> - Terme Fontana</option>
                 
                 
                <option id="bold" value="">• LINIJIA ŠT 9: Zrkovci - Dogoše</option>
                <option value="9.AP Mlinska"> - AP Mlinska</option>
                <option value="9.Citiy center"> - Citiy Center</option>
                <option value="9.Kneza koclja vetrinjska"> - Kneza Koclja - Vetrinjska</option>
                <option value="9.Magdalena"> - Magdalena</option>
                <option value="9.Pobreška europark"> - Pobreška - Europark</option>
                <option value="9.Greenwich"> - Greenwich</option>
                <option value="9.Čufarjeva TVD partizan"> - Čufarjeva - TVD Partizan</option>
                <option value="9.Čufarjeva dom st.občanov"> - Čufarjeva - dom st. občanov</option>
                <option value="9.Zrkovska GD"> - Zrkovska GD</option>
                <option value="9.Zrkovska cesta"> - Zrkovska cesta</option>
                <option value="9.Zrkovska trgovina"> - Zrkvoska trgovina</option>
                <option value="9.Zrkovci na terasi"> - Zrkvoci - Na terasi</option>
                <option value="9.Zrkovci obračališče"> - Zrkvoci - obračališe</option>
                <option value="9.Zrkovci na gorci 54"> - Zrkvoci - Na gorci 54</option>
                <option value="9.Zrkovci na gorci 65"> - Zrkvoci - Na gorci 65</option>
                <option value="9.Svenškova ulica 40"> - Svenškova ulica 40</option>
                <option value="9.Dupleška cesta 239"> - Dupleška cesta 239</option>
                <option value="9.Dogoše GD"> - Dogoše - GD</option>
                <option value="9.Dupleška cesta 255"> - Dupleška cesta 255</option>
                 
                 
                  <option id="bold" value="">• LINIJIA ŠT 10: Malečnik </option>
                <option value="10.Melje obračališče"> - Melje obračališče</option>
                <option value="10.Ul.kraljeviča marka"> - Ul.Kraljeviča Marka</option>
                <option value="10.Oreško nabrežje 1"> - Oreško nabrežje 1</option>
                <option value="10.Oreško nabrežje 2"> - Oreško nabrežje 2</option>
                <option value="10.AP mlinska"> - AP Mlinska</option>
                <option value="10.City center"> - City center</option>
                <option value="10.Kneza koclja vetrinjska"> - Kneza Koclja - Vetrinjska</option>
                <option value="10.Magdalena"> - Magdalena</option>
                <option value="10.Pobreška europark"> - Pobreška Europark</option>
                <option value="10.Greenwich"> - Greenwich</option>
                <option value="10.Čufarjeva TVD partizan"> - Čufarjeva - TVD Partizan</option>
                <option value="10.Čufarjeva dom st.občanov"> - Čufarjeva - dom st. občanov</option>
                <option value="10.Meljski hrib most"> - Meljski hrib - most</option>
                <option value="10.Kovačič"> - Kovačič</option>
                <option value="10.Lorber"> - Lorber</option>
                <option value="10.Malečnik odcep trčova"> - Malečnik - odcep Trčova</option>
                <option value="10.Malečnik trgovina"> - Malečnik trgovina</option>
                <option value="10.Trčova 31a"> - Trčova 31a</option>
                <option value="10.Trčova griček"> - Trčova - Griček</option>
                <option value="10.Novak"> - Novak</option>
                <option value="10.Metava duplek križišče"> - Metava - Duplek križišče</option>
                <option value="10.Metava obračališče"> - Metava - obračališče</option>
                 
                 
                 <option value="LINIJA 12 Dobrava-Pobrežje-AP Mlinska-Gosposvetska rondo-Dobrava "> "LINIJA 12 Dobrava-Pobrežje-AP Mlinska-Gosposvetska rondo-Dobrava</option>
                 <option value="-Pokopališče Dobrava - obračališče"> -Pokopališče Dobrava - obračališče</option>
                 <option value="-Dupleška - kanal "> -Dupleška - kanal </option>
                 
                 
                 <option id="bold" value="LINIJA 13 Črnogorska ">• LINIJA 13 Črnogorska</option>
                 <option value="Poštni center - obračališče"> -Poštni center - obračališče</option>
                 <option value="Tam - Nkbm"> -Tam - Nkbm</option>
                 <option value="Tam - črpalka"> -Tam - črpalka</option> 
                 <option value="Tam - Durabus"> -Tam - Durabus</option> 
                 <option value="Tam - vhod"> -Tam - vhod</option> 
                 <option value="Perhavčeva ul. - šola"> -Perhavčeva ul. - šola</option> 
                 <option value="Kavčičeva"> -Kavčičeva</option> 
                 <option value="Zagrebška - Šerbinek"> -Zagrebška - Šerbinek</option> 
                 <option value="Zagrebška - Kovinar"> -Zagrebška - Kovinar</option> 
                 <option value="ŽP Tezno"> -ŽP Tezno</option> 
                 <option value="Belokranjska ulica"> -Belokranjska ulica</option> 
                 <option value="Makedonska"> -Makedonska</option> 
                 <option value="Črnogorska št. 23"> -Črnogorska št. 23</option> 
                 <option value="Ob gozdu"> -Ob gozdu</option> 
                 <option value="Nasipna - Snaga"> -Nasipna - Snaga</option> 
                 <option value="Pobreška - Europark"> -Pobreška - Europark</option> 
                 <option value="Magdalena"> -Magdalena</option> 
                 <option value="Glavni trg - Židovska"> -Glavni trg - Židovska</option> 
                 <option value="City center"> -City center</option> 
                 <option value="AP Mlinska"> -AP Mlinska</option>
                 
                 
                 <option value="LINIJA 15 Bresternica "> LINIJA 15 Bresternica </option>
                 <option value="-Bresternica - obračališče">-Bresternica - obračališče</option>
                 <option value=" -Košaški dol"> -Košaški dol</option>
                 
                 
                 <option value="LINIJA 151 Gaj nad Mariborom ">LINIJA 151 Gaj nad Mariborom </option>
                 <option value=" -Gaj nad Mariborom"> -Gaj nad Mariborom</option>
                 <option value="-AP Mlinska"> -AP Mlinska</option>
                 
                 
                 <option value="LINIJA 16 Dogoše-Zg. Duplek ">LINIJA 16 Dogoše-Zg. Duplek</option>
                 <option value="-AP Mlinska"> -AP Mlinska</option>
                 <option value="-Zg.Duplek - obračališče"> -Zg.Duplek - obračališče</option>
                 
                 
                 <option value="LINIJA 17: Ribniško selo - Studenci ">LINIJA 17: Ribniško selo - Studenci </option>
                 <option value="-Ribniško selo - obračališče">-Ribniško selo - obračališče</option>
                 <option value="-Studenci - obračališče"> -Studenci - obračališče </option>
                 
                 
                 <option value="LINIJA 18 Pekre ">LINIJA 18 Pekre</option>
                 <option value="-Melje - obračališče">-Melje - obračališče</option>
                 <option value="-Pekre - trgovina"> -Pekre - trgovina </option>
                 
                 
                 <option value="LINIJA 19 Šarhova ">LINIJA 19 Šarhova</option>
                 <option value="-AP Mlinska"> AP Mlinska</option>
                 <option value=" Šarhova - obračališče">-Šarhova - obračališče </option>
                 
                 
                 <option value="LINIJA 20 Grušova "> LINIJA 20 Grušova</option>
                 <option value="-Melje - obračališče"> -Melje - obračališče</option>
                 <option value="-Grušova-obračališče"> -Grušova-obračališče </option>
                 
                 
                 <option value="LINIJA 21 Ljubljanska-Tržaška c. - Merkur ">LINIJA 21 Ljubljanska-Tržaška c. - Merkur</option>
                 <option value="-Melje - obračališče"> -Melje - obračališče</option>
                 <option value=" - Tržaška cesta - Merkur">-Tržaška cesta - Merkur </option>
            </select>
                  <br/><input type="submit" value="Potrdi" name="submit" id="gumb" />
      
        </form>   
             <input type="submit" value="Priljubljene Postaje" name="submit" id="gumb" />
    </body>
      </div>
    </div>
    <div id="footer">
      Copyright &copy; MMP 
          <script type="text/javascript">
             document.write ('<p><span id="date-time">', new Date().toLocaleString(), '<\/span>.<\/p>')
             if (document.getElementById) onload = function () {
	               setInterval ("document.getElementById ('date-time').firstChild.data = new Date().toLocaleString()", 50)
}
</script>
    </div>
  </div>
    </body>
</html>
