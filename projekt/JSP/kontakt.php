<!DOCTYPE HTML>
<html>

<head>

  <title>Kontakt</title>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link href="../CSS/style.css" rel="stylesheet" type="text/css"/>
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
          <input type="image" style="width: 450px" style= "height: 450px" style="align: center" src="../slike/logo_mmp.jpg" ></input>
      </div>
      <div id="menubar">
        <ul id="menu">
          <li> <a href="../JSP/index.php">Vozni redi</a></li>
          <li><a href="../HTML/cenik.html">Cenik</a></li>
          <li><a href="../HTML/mreza_linij.html">Mreža linij</a></li>
          <li><a href="../HTML/obvestila.html">Obvestila</a></li>
          <li> <a href="../HTML/o_strani.html">O spletni strani</a></li>
          <li><a href="../JSP/kontakt.php">Kontakt</a></li>
        </ul>
      </div>
    </div>
    <div id="site_content">
      <div class="sidebar">
        <h3>Povezave</h3>
        <p><a href="https://www.facebook.com/MMP-Mariborski-mestni-prevozi-1516327975359920/?fref=ts"><img src="../slike/index.png" alt="" width="42" height="42"/></a>- Facebook</p>
        <hr>
        <br>
        <p><a href="https://twitter.com/mestni_prevozi"><img src="../slike/twitter.png" alt="" width="42" height="42"/></a>- Twitter</p>     
        
        <h3>Iskanje</h3>
        <form method="post" action="#" id="search_form">
          <p>
            <input class="search" type="text" name="search_field" value="Vnesi besedilo..." />
            <input name="search" type="image" style="border: 0; margin: 0 0 -9px 5px;" src="../slike/search.png" alt="Search" title="Search" />
          </p>
        </form>
      </div>
      <div id="content">
        <h1>Kontakt</h1>
        <p>Tukaj vpišite podatke in napišite sporočilo:</p>
        <form action="Mail.php" method="post">
          <div class="form_settings">
            <p><span>Ime in priimek:</span><input name="name" type="text" /></p>
            <p><span>Email:</span><input name="email" type="text"/></p>
            <p><span>Sporočilo:</span><textarea  rows="8" cols="50" name="sporocilo"></textarea></p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input type="submit" name="submit" value="Submit" /></p>
            <?php
                    include("Mail.php");
            ?>       
          </div>
        </form>
      </div>
    </div>
    <div id="footer">
      Copyright &copy; MMP
      <script type="text/javascript">
             document.write ('<p><span id="date-time">', new Date().toLocaleString(), '<\/span>.<\/p>')
             if (document.getElementById) onload = function () {
	               setInterval ("document.getElementById ('date-time').firstChild.data = new Date().toLocaleString()", 50)
}
             </script>
    </div>
  </div>
</body>
</html>



